\documentclass[aspectratio=169]{beamer}
\usepackage{pdfpc-movie}
\usepackage{tikz}
\usepackage{tabularx}
\usepackage{hyperref}
\usetheme{Boadilla}
\usecolortheme{dove}

\title{New Concordances within the Notre Dame Repertory}
\subtitle{A Preliminary Report}
\author{Joshua Stutter}
\institute{University of Sheffield}
\date{July 6, 2024}
\titlegraphic{
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{images/image_0.png}}{images/titlegraphic.mp4}
}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}
\section{Musical reuse and the clausula}
\begin{frame}{Musical reuse and the clausula}
  \begin{minipage}{.6\textwidth}
    \begin{itemize}
      \item Variance and musical reuse is predominantly viewed through the concept of the clausula
      \item Clausula concordances were first catalogued by Friedrich Ludwig (1910)
      \item Questions:
        \begin{itemize}
          \item How do we know that we have found all of the interconnections?
          \item Does musical reuse go deeper?
        \end{itemize}
      \item Examples of \textit{clausulae} deriving from motets rather than the other way around (Rokseth 1939; Bradley 2013)
      \item Interrelationships between \textit{organa} without the help of \textit{clausulae} (Smith 1973)
      \item Links to \textit{conductus} (Bukofzer 1953; Everist 2018)
    \end{itemize}
  \end{minipage}%
  \begin{minipage}{.4\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=.6\linewidth]{images/F_f161r_crop.jpg}
      \caption{Pluteus 29.1, f.161r: page of substitute clausulae on `Ta'}
    \end{figure}
  \end{minipage}
\end{frame}
\subsection{A recently-found example}
\begin{frame}{A recently-found example}
  \begin{figure}
    \centering
    \includegraphics[width=.4\linewidth]{images/F_f143v_I.png}
    \caption{Pluteus 29.1, f.143v, I: `Patrum' from \textit{Alleluya.\ Benedictus es} (M57)}
  \end{figure}
  \begin{minipage}{.5\textwidth}
    \uncover<2->{\begin{figure}
      \centering
      \includegraphics[width=.7\linewidth]{images/F_f6v_III.png}
      \caption{Pluteus 29.1, f.6v, III: `Domine' from \textit{Sederunt principes.\ Adiuva me} (M3)}
    \end{figure}}
  \end{minipage}%
  \begin{minipage}{.5\textwidth}
    \uncover<3->{\begin{figure}
      \centering
      \includegraphics[width=.7\linewidth]{images/F_f1v_I.png}
      \caption{Pluteus 29.1, f.1v, I: `Viderunt' from \textit{Viderunt omnes.\ Notum fecit} (M1)}
    \end{figure}}
  \end{minipage}
\end{frame}
\begin{frame}{iuchair}
  \begin{figure}
    \centering
    \includegraphics[width=.7\linewidth]{images/iuchairjan2022.jpg}
    \caption{\textit{iuchair}, January 2022}
  \end{figure}
\end{frame}
\begin{frame}{Musical transformations make this difficult}
  \begin{itemize}
    \item We can look at large concordances between settings quite easily
    \item Smaller borrowings are more difficult to detect
    \item Music may have undergone any number of transformations in its borrowing
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[width=.7\linewidth]{images/F_f150v_III.png}
    \caption{Pluteus 29.1, f.150v, III: `Nusmido' substitute clausula}
  \end{figure}
\end{frame}
\section{`Some type of machinery'}
\begin{frame}{`Some type of machinery'}
  \begin{minipage}{.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=.5\linewidth]{images/busa1965.jpg}
      \caption{Fr Roberto Busa (1913--2011), c.1965 (Busa archive)}
    \end{figure}
  \end{minipage}%
  \begin{minipage}{.5\textwidth}
    \begin{figure}
      \centering
      \pdfpcmovie[autostart, loop]{\includegraphics[width=.5\textwidth]{images/tischler_pageflick.jpg}}{images/tischler_pageflick.mp4}
      \caption{Hans Tischler's (1988) \textit{Complete Comparative Edition}}
    \end{figure}
  \end{minipage}
\end{frame}
\section{The Clausula Archive of the Notre Dame Repertory (CANDR)}
\begin{frame}{CANDR}
  \vskip-25pt
  {
    \huge
    \begin{table}
      \centering
      \setlength\tabcolsep{0pt}
      \begin{tabular}{r l}
        \textbf{C} & lausula \\
        \textbf{A} & rchive \textit{of the} \\
        \textbf{N} & otre \\
        \textbf{D} & ame \\
        \textbf{R} & epertory \\
      \end{tabular}
      \setlength\tabcolsep{6pt}
    \end{table}
    \begin{center}
      \large
      \url{https://candr.org.uk}
    \end{center}
  }
  \begin{enumerate}
    \item Database and tracing tool
    \item Software for transcribing graphical data into symbolic music notation
    \item Programming toolkit for analysis
  \end{enumerate}
\end{frame}
\subsection{Database}
\begin{frame}<1>[label=databasepar]
  \frametitle{Database}
  \begin{itemize}
    \item<1-> Free and open source
    \item<2-> Contains no symbolic data, only graphical information akin to rubbings of the parchment
    \item<3-> Optical music recognition (OMR) based on the method of pixel classification using convolutional neural networks after Calvo-Zaragoza et al. (2018)
    \item<4-> OMR results are proofread and corrected before being fed back into the system as ``ground truth'' for subsequent OMR
    \item<5-> Database currently contains:
      \begin{itemize}
        \item Entirety of D-W Cod. Guelf. 628 Helmst.
        \item Entirety of D-W Cod. Guelf. 1099 Helmst.
        \item Sample of substitute clausulae from I-Fl Pluteus 29.1
      \end{itemize}
    \item<5-> 466 folios, 5,626 systems, 9,710 staves, 560,293 notational elements
  \end{itemize}
\end{frame}
\begin{frame}{Database}
  \begin{figure}
    \centering
    \pdfpcmovie[autostart]{\includegraphics[width=.7\textwidth]{images/deepdiving.png}}{images/deepdiving.mp4}
    \caption{Source to folio to facsimile to system to stave to notation}
  \end{figure}
\end{frame}
\againframe<2>{databasepar}
\begin{frame}{Database}
  \begin{figure}
    \centering
    \pdfpcmovie[autostart]{\includegraphics[width=.7\textwidth]{images/inputprocess.png}}{images/inputprocess.mp4}
    \caption{Source to folio to facsimile to system to stave to notation}
  \end{figure}
\end{frame}
\againframe<3->{databasepar}
\subsection{Symbolic transcription}
\begin{frame}{Symbolic transcription}
  \begin{itemize}
    \item Plugin system for transcribing graphical notation into symbolic data
    \item Encoding systems try to eliminate ambiguity rather than represent it
    \item Customisation based upon the schema of the \textit{Music Encoding Initative} (MEI) (Music Encoding Initiative 2024)
    \item Aligning voices via the \texttt{@synch} attribute rather than by specifying durations
    \item Representation of original notation rather than transcription ``in modern notes''
  \end{itemize}
\end{frame}
\subsection{Programming toolkit}
\begin{frame}{Programming toolkit}
  \begin{itemize}
    \item Python library: scrape, preprocess, analyse MEI encodings
    \item Current libraries such as \textit{music21} (Cuthbert et al. 2023) are not suitable
  \end{itemize}
\end{frame}
\begin{frame}{Programming toolkit}
  \begin{minipage}{.3\textwidth}
    \begin{figure}
      \centering
      \begin{tikzpicture}
        \node [anchor=south west, inner sep=0] (image) at (0, 0) {\includegraphics[width=.4\linewidth]{images/tabularrepresentation.pdf}};
        \begin{scope}
          \path (image.south west) -- (image.north east) coordinate[pos=0.5] (center);
          \path (image.south west) coordinate (sw);
          \path (image.north east) coordinate (ne);
        \end{scope}
        \uncover<2-> {
          \draw[red, line width=3pt] (sw) -- (ne);
          \draw[red, line width=3pt] (sw |- ne) -- (ne |- sw);
        }
      \end{tikzpicture}
      \caption{Tabular representation of polyphony}
    \end{figure}
  \end{minipage}%
  \begin{minipage}{.8\textwidth}
    \begin{figure}
        \includegraphics<2>[width=.7\textwidth]{images/notation-graph-01.pdf}
        \only<2>{\caption{Undirected graph representation of polyphony}}
        \includegraphics<3->[width=.7\textwidth]{images/notation-graph-02.pdf}
        \only<3->{\caption{Complete undirected graph representation of polyphony}}
    \end{figure}
  \end{minipage}
\end{frame}
\begin{frame}{Programming toolkit}
  \begin{table}
    \tiny
    \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}} l|llllllllllllllll}
      & \textbf{AN} & \textbf{AS} & \textbf{BN} & \textbf{BS} & \textbf{CN} & \textbf{CS} & \textbf{DN} & \textbf{DS} & \textbf{EN} & \textbf{ES} & \textbf{FN} & \textbf{FS} & \textbf{GN} & \textbf{GS} & \textbf{DivisioneN} & \textbf{DivisoneS} \\
      \hline
      \textbf{A} & 0.40 & 0.67 & 0.84 & 0.25 & 0.08 & 0.77 & 1.00 & 0.22 & 0.59 & 0.24 & 0.61 & 0.83 & 0.59 & 0.73 & 0.54 & 0.06 \\
      \textbf{B} & 0.94 & 0.35 & 0.77 & 0.99 & 1.00 & 1.00 & 0.75 & 0.37 & 0.87 & 0.44 & 0.60 & 0.98 & 0.13 & 0.08 & 0.66 & 0.26 \\
      \textbf{C} & 0.19 & 0.99 & 0.59 & 0.48 & 0.69 & 0.90 & 0.24 & 0.14 & 0.08 & 0.77 & 0.08 & 0.23 & 0.60 & 1.00 & 0.21 & 0.48 \\
      \textbf{D} & 0.11 & 0.03 & 0.02 & 0.53 & 0.63 & 0.99 & 0.88 & 0.85 & 0.95 & 0.78 & 0.34 & 0.65 & 0.60 & 0.35 & 0.66 & 0.91 \\
      \textbf{E} & 0.21 & 0.46 & 0.94 & 0.46 & 0.95 & 0.17 & 0.78 & 0.15 & 0.88 & 0.64 & 0.60 & 0.95 & 0.93 & 0.62 & 0.94 & 0.89 \\
      \textbf{F} & 0.66 & 0.23 & 0.76 & 0.42 & 0.30 & 0.34 & 0.86 & 0.85 & 0.88 & 0.38 & 0.14 & 0.72 & 0.56 & 0.06 & 0.79 & 0.32 \\
      \textbf{G} & 0.66 & 0.43 & 0.12 & 0.11 & 0.50 & 0.13 & 0.30 & 0.12 & 0.27 & 0.42 & 0.44 & 0.99 & 0.61 & 0.45 & 0.90 & 0.65 \\
      \textbf{Divisione} & 0.82 & 0.33 & 0.26 & 0.25 & 0.08 & 0.26 & 0.28 & 0.58 & 0.70 & 0.32 & 0.88 & 0.93 & 0.46 & 0.73 & 0.50    & 0.19
    \end{tabular*}
    \caption{Example vector representation of notational elements}
  \end{table}
  \begin{center}
    For further information, see Stutter (forthcoming).
  \end{center}
\end{frame}
\section{Initial results}
\begin{frame}
  \frametitle{The whole data set}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{images/boomerang_whole_set_rotate_nosound_edit_withlegend.png}}{images/boomerang_whole_set_rotate_nosound_edit_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{The divisione element}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{images/boomerang_divisione_rotate_v2_nosound_withlegend.png}}{images/boomerang_divisione_rotate_v2_nosound_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{Distribution of clefs}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.6\textwidth]{images/boomerang_clefs_rotate_nosound_edit.png}}{images/boomerang_clefs_rotate_nosound_edit.mp4}
\end{frame}
\begin{frame}
  \frametitle{Distribution of ligated vs unligated notes}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{images/boomerang_ligated_unligated_rotate_nosound_withlegend.png}}{images/boomerang_ligated_unligated_rotate_nosound_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{Whole-setting reuse}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{images/boomerang_95_472_427_655_rotate_nosound_edit_withlegend.png}}{images/boomerang_95_472_427_655_rotate_nosound_edit_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{Stylometric analysis}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{images/boomerang_chancellor_655_656_175_229_213_243_244_245_VS_unrelated_655_654_658_663_664_VS_48_crucifigat_rotate_withlegend.png}}{images/boomerang_chancellor_655_656_175_229_213_243_244_245_VS_unrelated_655_654_658_663_664_VS_48_crucifigat_rotate_withlegend.mp4}
\end{frame}
\section{Limitations and conclusions}
\begin{frame}{Limitations and conclusions}
  \begin{itemize}
    \item Limitations:
      \begin{itemize}
        \item Not yet a complete process for analysing musical reuse, still a relatively blunt tool
        \item We want to be able to measure these patterns to determine significance
        \item May be remedied by a more robust embedding methodology
      \end{itemize}
    \item Conclusions:
      \begin{itemize}
        \item Largest and most comprehensive database of Notre Dame polyphony currently available, entirely open source
        \item Developing analytical frameworks for ``distant reading'' of thirteenth-century polyphony
        \item A methodology that should be expanded to further data to understand more about musical reuse within the Notre Dame repertory
      \end{itemize}
  \end{itemize}
\end{frame}
\section{Thank you}
\begin{frame}{Thank you}
  \scriptsize{
    Bradley, C.A.\ (2013) `Contrafacta and Transcribed Motets: Vernacular Influences on Latin Motets and Clausulae in the Florence Manuscript', \textit{Early Music History}, 32, pp.1--70.\\
    Bukofzer, M.\ (1953) `Interrelations between Conductus and Clausula', \textit{Annales Musicologiques} 1, pp.65--103.\\
    Calvo-Zaragoza, J., Vigliensoni, G., and Fujinaga, I.\ (2018) `Pixelwise classification of music document analysis', \textit{Proceedings of the Seventh International Conference on Image Processing Theory, Tools and Applications (IPTA)}. Available at: \url{https://doi.org/10.1109/IPTA.2017.8310134}.\\
    Cuthbert, M.S.A.\ (2023) \textit{music21: A Toolkit for Computer-Aided Musical Analysis and Computational Musicology}. Available at: \url{https://github.com/cuthbertLab/music21} (Accessed: 3 July 2024).\\
    Everist, M.\ (2018) \textit{Discovering Medieval Song}. Cambridge University Press.\\
    Ludwig, F.\ (1910) \textit{Repertorium Organum Recentioris et Motetorum Vetustissimi Stili} ed.\ Dittmer, L.A. New York: Institute of Mediæval Music.\\
    Music Encoding Initiative (2024) \textit{Music Encoding Initiative}. Available at: \url{https://music-encoding.org/} (Accessed: 3 July 2024).\\
    Rokseth, Y. (1935--39) \textit{Polyphonies du xiii\textsuperscript{e}: Le manuscrit H196 de la Faculté de médecine de Montpellier}. (4 vols). Paris: Editions de l'Oiseau Lyre.\\
    Smith, N.E.\ (1973) `Interrelationships among the Graduals of the Magnus Liber Organi', \textit{Acta Musicologica} 45(1), pp.73--97. Available at: \url{https://doi.org/10.2307/932223}.\\
    Stutter, J.\ (forthcoming) `Encoding Notre Dame polyphony for corpus analysis' \textit{Journal of the Text Encoding Initiative}.\\
    Tischler, H.\ (1988) \textit{The Parisian Two-part Organa: the Complete Comparative Edition}. Stuyvesant: Pendragon Press.\\
  }
\end{frame}

\end{document}
